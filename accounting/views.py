from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
from accounting.models import *


def login(request):
    return render(request, "accounting/login.html")


def dashboard(request):
    return render(request, 'accounting/dashboard.html')


def customers(request):
    customer_type = request.GET['type']
    customers = Customer.objects.filter(customer_type=customer_type)
    data = {
        'pagetitle': 'Customers',
        'customers': customers
    }
    return render(request, 'accounting/customers.html', data)


def transactions(request):
    return render(request, 'accounting/transactions.html')


def logs(request):
    return render(request, 'accounting/logs.html')