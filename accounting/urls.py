from django.urls import path

from django.contrib.auth import views as auth_views

from accounting import views

urlpatterns = (
    path('dashboard/', views.dashboard, name='dashboard'),
    path('login/', views.login, name='login'),
    path('customers/', views.customers, name='customers'),
    path('transactions/', views.transactions, name='transactions'),
    path('logs/', views.logs, name='logs'),

)
