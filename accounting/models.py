from django.db import models


class Customer(models.Model):
    TYPE = [
        ('gen_customer', 'General Customer'),
        ('subdealer', 'Subdealer'),
    ]
    customer_type = models.CharField(choices=TYPE, max_length=255, blank=True, null=True)
    firstname = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    subdealer = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    contact_no = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.firstname + ' ' + self.lastname + ' ' + self.subdealer


class Transaction(models.Model):
    BRANCH = [
        ('cabanatuan', 'Cabanatuan'),
        ('san jose', 'San Jose')
    ]

    STATUS = [
        ('paid', 'Paid'),
        ('unpaid', 'Unpaid')
    ]
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)
    branch = models.CharField(max_length=255, null=True, blank=True, choices=BRANCH, default='cabanatuan')
    invoice_no = models.CharField(max_length=255, null=True, blank=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    delivery_receipt_no = models.CharField(max_length=255, null=True, blank=True)
    terms = models.CharField(max_length=255, null=True, blank=True)
    purchase_date = models.DateField()
    date_due = models.DateField()
    remarks = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=20, null=True, blank=True, choices=STATUS, default='paid')
    date_updated = models.DateField(null=True, blank=True)
    date_added = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.customer.firstname + ' ' + self.customer.lastname + ' ' + self.customer.subdealer


class Payment(models.Model):
    PAYMENT_TYPE = [
        ('cash', 'Cash'),
        ('check', 'Check'),
        ('online', 'Online'),
    ]

    DEPOSIT_TYPE = [
        ('cash', 'Cash'),
        ('check', 'Check'),
    ]

    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)
    transaction = models.ForeignKey(Transaction, on_delete=models.SET_NULL, blank=True, null=True)
    payment_type = models.CharField(max_length=255, choices=PAYMENT_TYPE, default="")
    amount_paid = models.CharField(max_length=255, blank=True, null=True)
    check_no = models.IntegerField(blank=True, null=True)
    check_date = models.DateField(blank=True, null=True)
    date_given = models.DateField(blank=True, null=True)
    deposit_type = models.CharField(max_length=255, choices=DEPOSIT_TYPE, default="cash")
    deposit_date = models.DateField(blank=True, null=True)
    bank = models.CharField(max_length=255, blank=True, null=True)
    payment_date = models.DateField(blank=True, null=True)
    notes = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.customer.firstname + ' ' + self.customer.lastname + ' ' + self.customer.subdealer
