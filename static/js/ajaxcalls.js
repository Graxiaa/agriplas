

// login
$("#login").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);  
    var message =  $("#login-alert");

    $.ajax({
            type: "POST",
            dataType: "json",  
            url: 'controllers/ajax.php',
            data: { 
               "action": "login",
               "method": "POST",
               "form": form.serialize(),
            }, 
            beforeSend: function() {
                $('input').removeClass('border border-danger');
                message.hide();
            },
            success: function(res)
            {
                

                if(res.status == 0){
                    message.text(res.message).show();

                    if(res.err_class){ 
                        var classname = res.err_class  
                        for (var i = 0; i < classname.length; ++i) {
                            console.log()
                            $("#" + classname[i]).addClass("border border-danger");
                        }
                    }
                }else{
                    window.location.href = res.success_url;
                }

                
            },
            error: function(xhr) { // if error occured
                console.log(xhr)
            },
         });

    
});

// create general customer
$("#gen_customer_add_form").submit(function(e) { 
    e.preventDefault(); // avoid to execute the actual submit of the form.
   
    var form = $(this);  
    var message =  $("#alert");

    $.ajax({
        type: "POST",
        dataType: "json",  
        url: 'controllers/ajax.php',
        data: { 
            "action": "add_gen_customer",
            "method": "POST",
            "form": form.serialize(),
        }, 
        beforeSend: function() {
            $("#alert").removeClass('alert-danger alert-success').hide()
            $('input').removeClass('border border-danger');
            message.hide();
        },
        success: function(res)
        {  
            if(res.status == 0){
                message.addClass('alert-danger').text(res.message).show();

                if(res.err_class){ 
                    var classname = res.err_class  
                    for (var i = 0; i < classname.length; ++i) {
                        console.log()
                        $("#" + classname[i]).addClass("border border-danger");
                    }
                } 
            }
            
            if(res.status == 1){
                message.addClass('alert-success').text(res.message).show()
                $('input').val('')
            }

            
        },
        error: function(xhr) { // if error occured
            console.log('error')
        },
        }); 
});

//create subdealer
$("#subdealer_add_form").submit(function(e) { 
    e.preventDefault(); // avoid to execute the actual submit of the form.
   
    var form = $(this);  
    var message =  $("#alert");

    $.ajax({
        type: "POST",
        dataType: "json",  
        url: 'controllers/ajax.php',
        data: { 
            "action": "add_subdealer",
            "method": "POST",
            "form": form.serialize(),
        }, 
        beforeSend: function() {
            $("#alert").removeClass('alert-danger alert-success').hide()
            $('input').removeClass('border border-danger');
            message.hide();
        },
        success: function(res)
        {  
            if(res.status == 0){
                message.addClass('alert-danger').text(res.message).show();

                if(res.err_class){ 
                    var classname = res.err_class  
                    for (var i = 0; i < classname.length; ++i) {
                        console.log()
                        $("#" + classname[i]).addClass("border border-danger");
                    }
                } 
            }
            
            if(res.status == 1){
                message.addClass('alert-success').text(res.message).show()
                $('input').val('')
            }

            
        },
        error: function(xhr) { // if error occured
            console.log('error')
        },
    });
});


//create gen customer transaction
$("#add_gen_customer_transaction").submit(function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    console.log("submit gen customer form")
    var form = $(this);  
    var message =  $("#alert");

    $.ajax({
        type: "POST",
        dataType: "json",  
        url: 'controllers/ajax.php',
        data: { 
            "action": "add_gen_customer_transaction",
            "method": "POST",
            "form": form.serialize(),
        }, 
        beforeSend: function() {
            $("#alert").removeClass('alert-danger alert-success').hide()
            $('input, select').removeClass('border border-danger');
            message.hide();
        },
        success: function(res)
        {  
            if(res.status == 0){
                message.addClass('alert-danger').text(res.message).show();

                if(res.err_class){ 
                    var classname = res.err_class  
                    for (var i = 0; i < classname.length; ++i) { 
                        $("#" + classname[i]).addClass("border border-danger");
                    }
                } 
            }
            
            if(res.status == 1){
                message.addClass('alert-success').text(res.message).show()
                $('input, select').val('')
            }

            
        },
        error: function(xhr) { // if error occured
            console.log('error')
        },
    });
});


//create gen customer transaction
$("#add_subdealer_transaction").submit(function(e){
    e.preventDefault(); // avoid to execute the actual submit of the form.
    console.log("submit subdealer form")
    var form = $(this);  
    var message =  $("#alert");

    $.ajax({
        type: "POST",
        dataType: "json",  
        url: 'controllers/ajax.php',
        data: { 
            "action": "add_subdealer_transaction",
            "method": "POST",
            "form": form.serialize(),
        }, 
        beforeSend: function() {
            $("#alert").removeClass('alert-danger alert-success').hide()
            $('input, select').removeClass('border border-danger');
            message.hide();
        },
        success: function(res)
        {  
            if(res.status == 0){
                message.addClass('alert-danger').text(res.message).show();

                if(res.err_class){ 
                    var classname = res.err_class  
                    for (var i = 0; i < classname.length; ++i) { 
                        $("#" + classname[i]).addClass("border border-danger");
                    }
                } 
            }
            
            if(res.status == 1){
                message.addClass('alert-success').text(res.message).show()
                $('input, select').val('')
            }

            
        },
        error: function(xhr) { // if error occured
            console.log('error')
        },
    });
});




//create gen customer transaction
$("#payment_form").submit(function(e){
    console.log("pay")
    e.preventDefault(); // avoid to execute the actual submit of the form. 
    var form = $(this);  
    var message =  $(this).find('.alert');

    $.ajax({
        type: "POST",
        dataType: "json",  
        url: 'controllers/ajax.php',
        data: { 
            "action": "pay_transaction",
            "method": "POST",
            "form": form.serialize(), 
        }, 
        beforeSend: function() {
            $('input, select').removeClass('border border-danger');
            message.removeClass('alert-danger alert-success').hide() 
            
        },
        success: function(res)
        {  
            if(res.status == 0){
                message.addClass('alert-danger').text(res.message).show();

                if(res.err_class){ 
                    var classname = res.err_class  
                    for (var i = 0; i < classname.length; ++i) { 
                        $("#" + classname[i]).addClass("border border-danger");
                    }
                } 
            }
            
            if(res.status == 1){
                message.addClass('alert-success').text(res.message).show()
                location.reload()
                // $('input, select').val('') 
                // $('.close').trigger("click")
                // console.log("hide modal 2")
            }

            
        },
        error: function(xhr) { // if error occured
            console.log('error')
        },
    });
});