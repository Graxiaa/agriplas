// AOS.init(); 
$(document).ready( function () {


    $('#transactions-table').DataTable({
        /*"paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,*/
        "responsive": true,
    });

    $('#customers-table').DataTable({
        /*"paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,*/
        "responsive": true,
    });

    $('#paymentHistoryTable').DataTable(
        {
            "order": [[ 0, "asc" ]]
        }
    );


});


$(".customer_type").change(function(e){
    $(".customer_fields").hide()
    if($(this).val() == 'gen_customer'){
        $(".gen_cust_field").show()
    }else{
        $(".subdealer_field").show()
    }
});


 
$("input[name=mode_of_payment]").change(function(e){
    var amount_type = "cash"
    var mop = $(this).val()

    $(".field_set").hide() 
    if(mop == 'cash'){
        amount_type = "Cash"
        $(".cash_field").show()
    }
    if(mop == 'check'){
        amount_type = "Check"
        $(".check_field").show()
    }
    if(mop == 'online'){
        amount_type = "Deposit"
        $(".online_field").show()
    }
    $("#amount_type").text(amount_type)
});

$(".pay_btn").click(function(e){  
    $("input[name=transaction_id]").val($(this).data("id"))
})


$(".close").click(function(e){
    $(".alert").hide()
})


$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

$(".number_format").digits();